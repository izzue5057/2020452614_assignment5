function validateForm() {

                var name = document.forms["myForm"]["name"].value;
                var length = document.forms["myForm"]["length"].value;
                var width = document.forms["myForm"]["width"].value;
                var height = document.forms["myForm"]["height"].value;
                var mode = document.querySelector('input[name="mode"]:checked').value; 
                var type = document.querySelector('input[name="type"]:checked').value; 
                var cost=0;
                var myname = name.toUpperCase();

                weight = (length*width*height)/5000; //weight

                if (type =="Domestic") {
                    if(weight < 2) {
                        if(mode =="Surface")
                            cost = 7;
                        else if(mode=="Air")
                            cost = 10;
                    }
                    else {
                        if(mode =="Surface")
                            cost = 7 + ((weight-2)*1.5);
                        else if(mode =="Air")
                            cost = 10 + ((weight-2)*3);
                    }
                }
                else if (type =="International") {
                    if(weight < 2) {
                        if(mode =="Surface")
                            cost = 20;
                        else if(mode =="Air")
                            cost = 50;
                    }
                }
                else {
                    if(mode =="Surface")
                        cost = 7 + ((weight-2)*3);
                    else if(mode == "Air")
                            cost = 10 + ((weight-2)*5);
                }
                
                confirm("Parcel Volumetric and Cost Calculator" +
                        "\nCustomer name: " + myname + 
                        "\nLength: " + length + " cm" +
                        "\nWidth: " + width + " cm" +
                        "\nHeight: " + height + " cm" +
                        "\nWeight: " + weight + " kg" +
                        "\nMode: " + mode + 
                        "\nType: " + type +
                        "\nDelivery cost: RM " + cost);   
            }

            function reset_msg()
            {
                confirm("The inputs will be reset");
            }